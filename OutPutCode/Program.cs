﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace OutPutCode
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamWriter sw = new StreamWriter("code.txt", false);
            // 各Cube类型门数量
            int[] typeGateCount = new int[9] { 4, 12, 12, 8, 8, 8, 8, 12, 12 };
            // 各Cube类型各方向门数量
            int[][] sideGateCount = new int[9][]
            {
                new int [4] { 1, 1, 1, 1 },
                new int [4] { 3, 3, 3, 3 },
                new int [4] { 3, 3, 3, 3 },
                new int [4] { 3, 1, 3, 1 },
                new int [4] { 1, 3, 1, 3 },
                new int [4] { 3, 1, 3, 1 },
                new int [4] { 1, 3, 1, 3 },
                new int [4] { 3, 3, 3, 3 },
                new int [4] { 3, 3, 3, 3 }
            };
            // 各Cube边的每个门相对（反方向边）的第一个门序号
            int[][] faceSideStart = new int[9][]
            {
                new int [4] { 0, 1, 2, 3 },
                new int [4] { 0, 3, 6, 9 },
                new int [4] { 0, 3, 6, 9 },
                new int [4] { 0, 3, 4, 7 },
                new int [4] { 0, 1, 4, 5 },
                new int [4] { 0, 3, 4, 7 },
                new int [4] { 0, 1, 4, 5 },
                new int [4] { 0, 3, 6, 9 },
                new int [4] { 0, 3, 6, 9 }
            };
            // 门反向方向序号
            int[][] oppositesIndex = new int[9][]
            {
                new int [12] { 2, 3, 0, 1, -1, -1, -1, -1, -1, -1, -1, -1 },
                new int [12] { 2, 2, 2, 3, 3, 3, 0, 0, 0, 1, 1, 1 },
                new int [12] { 2, 2, 2, 3, 3, 3, 0, 0, 0, 1, 1, 1 },
                new int [12] { 2, 2, 2, 3, 0, 0, 0, 1, -1, -1, -1, -1 },
                new int [12] { 2, 3, 3, 3, 0, 1, 1, 1, -1, -1, -1, -1 },
                new int [12] { 2, 2, 2, 3, 0, 0, 0, 1, -1, -1, -1, -1 },
                new int [12] { 2, 3, 3, 3, 0, 1, 1, 1, -1, -1, -1, -1 },
                new int [12] { 2, 2, 2, 3, 3, 3, 0, 0, 0, 1, 1, 1 },
                new int [12] { 2, 2, 2, 3, 3, 3, 0, 0, 0, 1, 1, 1 }
            };
            // 门记录排列序号（不满12）到全门类型序号（12个）转换
            int[][] translate = new int[9][]
            {
                new int [12] { 1, 4, 7, 10, -1, -1, -1, -1, -1, -1, -1, -1 },
                new int [12] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 },
                new int [12] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 },
                new int [12] { 0, 1, 2, 4, 6, 7, 8, 10, -1, -1, -1, -1 },
                new int [12] { 1, 3, 4, 5, 7, 9, 10, 11, -1, -1, -1, -1 },
                new int [12] { 0, 1, 2, 4, 6, 7, 8, 10, -1, -1, -1, -1 },
                new int [12] { 1, 3, 4, 5, 7, 9, 10, 11, -1, -1, -1, -1 },
                new int [12] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 },
                new int [12] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 }
            };
            // 全门类型序号（12个）到门记录排列序号（不满12）转换
            int[][] untranslate = new int[9][]
            {
                new int [12] { -1, 0, -1, -1, 1, -1, -1, 2, -1, -1, 3, -1 },
                new int [12] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 },
                new int [12] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 },
                new int [12] { 0, 1, 2, -1, 3, -1, 4, 5, 6, -1, 7, -1 },
                new int [12] { -1, 0, -1, 1, 2, 3, -1, 4, -1, 5, 6, 7 },
                new int [12] { 0, 1, 2, -1, 3, -1, 4, 5, 6, -1, 7, -1  },
                new int [12] { -1, 0, -1, 1, 2, 3, -1, 4, -1, 5, 6, 7 },
                new int [12] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 },
                new int [12] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 }
            };
            // 各个类型门对应的边类型：0:16,1-3:32,4-6:64
            int[][] sideType = new int[9][]
            {
                new int [12] { 0, 0, 0, 0, -1, -1, -1, -1, -1, -1, -1, -1 },
                new int [12] { 1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3 },
                new int [12] { 4, 5, 6, 4, 5, 6, 4, 5, 6, 4, 5, 6 },
                new int [12] { 1, 2, 3, 0, 1, 2, 3, 0, -1, -1, -1, -1 },
                new int [12] { 0, 1, 2, 3, 0, 1, 2, 3, -1, -1, -1, -1 },
                new int [12] { 4, 5, 6, 0, 4, 5, 6, 0, -1, -1, -1, -1 },
                new int [12] { 0, 4, 5, 6, 0, 4, 5, 6, -1, -1, -1, -1 },
                new int [12] { 4, 5, 6, 1, 2, 3, 4, 5, 6, 1, 2, 3 },
                new int [12] { 1, 2, 3, 4, 5, 6, 1, 2, 3, 4, 5, 6 }
            };
            // 两门连接类型通行关系
            string[][] linkAble = new string[7][]
            {
                new string [7] { "true", "true", "false", "true", "false", "false", "false" },
                new string [7] { "true", "true", "false", "true", "false", "false", "false" },
                new string [7] { "false", "false", "true", "false", "true", "true", "true" },
                new string [7] { "true", "true", "false", "true", "false", "false", "false" },
                new string [7] { "false", "false", "true", "false", "true", "true", "true" },
                new string [7] { "false", "false", "true", "false", "true", "true", "true" },
                new string [7] { "false", "false", "true", "false", "true", "true", "true" },
            };
            // 12个门坐标偏移倍率
            double[][] rate = new double[12][]
            {
                new double [2] { -0.5, -1 },
                new double [2] { 0, -1 },
                new double [2] { 0.5, -1 },
                new double [2] { 1, -0.5 },
                new double [2] { 1, 0 },
                new double [2] { 1, 0.5 },
                new double [2] { 0.5, 1 },
                new double [2] { 0, 1 },
                new double [2] { -0.5, 1 },
                new double [2] { -1, 0.5 },
                new double [2] { -1, 0 },
                new double [2] { -1, -0.5 }
            };
            // 各Cube类型边长
            double[][] side = new double[9][]
            {
                new double [2] { 16, 16 },
                new double [2] { 32, 32 },
                new double [2] { 64, 64 },
                new double [2] { 32, 16 },
                new double [2] { 16, 32 },
                new double [2] { 64, 16 },
                new double [2] { 16, 64 },
                new double [2] { 64, 32 },
                new double [2] { 32, 64 }
            };
            // 各方向正负值
            double[][] valueFace = new double[4][]
            {
                new double [2] { -1, -1 },
                new double [2] { 1, -1 },
                new double [2] { 1, 1 },
                new double [2] { -1, 1 }
            };
            // 各方向正负值
            double[][] sideOrder = new double[6][]
            {
                new double [3] { 0, 1, 2 },
                new double [3] { 0, 2, 1 },
                new double [3] { 1, 0, 2 },
                new double [3] { 1, 2, 0 },
                new double [3] { 2, 1, 0 },
                new double [3] { 2, 0, 1 },
            };

            sw.WriteLine("void LibEditor_FUNC_RWS_InitialFunction()\r\n{");
            
            sw.WriteLine("// Link relation of Gate side type ");

            for (int i = 0; i < 7; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    sw.WriteLine("libEditor_GV_RWS_ConcentrateModeLinkRelation[" + (i).ToString() + "][" + j.ToString() + "] = " + linkAble[i][j] + ";");
                }
            }

            sw.WriteLine("\r\n// Check for this cube in  concentrate mode ");

            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < typeGateCount[i]; j++)
                {
                    sw.WriteLine("libEditor_GV_RWS_ConcentrateModeCubeCheckValue[" + (i).ToString() + "][" + j.ToString() + "] = " + (sideType[i][j]).ToString() + ";");
                }
            }

            sw.WriteLine("\r\n// Link Gate priority order");
            sw.WriteLine("libEditor_GV_RWS_CubeSideRandomValue[0] = -1;");
            sw.WriteLine("libEditor_GV_RWS_CubeSideRandomValue[1] = 0;");
            sw.WriteLine("libEditor_GV_RWS_CubeSideRandomValue[2] = 2;");
            sw.WriteLine("libEditor_GV_RWS_CubeSideRandomValue[3] = 8;");
            sw.WriteLine("libEditor_GV_RWS_CubeSideRandomPriorityOrder[0][0] = 0;");
            sw.WriteLine("libEditor_GV_RWS_CubeSideRandomPriorityOrder[1][0] = 0;");
            sw.WriteLine("libEditor_GV_RWS_CubeSideRandomPriorityOrder[1][1] = 1;");
            sw.WriteLine("libEditor_GV_RWS_CubeSideRandomPriorityOrder[2][0] = 1;");
            sw.WriteLine("libEditor_GV_RWS_CubeSideRandomPriorityOrder[2][1] = 0;");
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    sw.WriteLine("libEditor_GV_RWS_CubeSideRandomPriorityOrder[" + (i + 3).ToString() + "][" + j.ToString() + "] = " + (sideOrder[i][j]).ToString() + ";");
                }
            }

            sw.WriteLine("\r\n// Set Side Length");

            for (int i = 0; i < 9; i++)
            {
                sw.WriteLine("libEditor_GV_RWS_CubeSideLength[" + i.ToString() + "][0] = " + (side[i][0]).ToString() + ";");
                sw.WriteLine("libEditor_GV_RWS_CubeSideLength[" + i.ToString() + "][1] = " + (side[i][1]).ToString() + ";");
            }

            sw.WriteLine("\r\n// From Cube record Gate index to 12 Gate index");

            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 12; j++)
                {
                    sw.WriteLine("libEditor_GV_RWS_CubeInTypeGateIndexToIndex[" + i.ToString() + "][" + j.ToString() + "] = " + (untranslate[i][j]).ToString() + ";");
                }
            }

            sw.WriteLine("\r\n// Count of Gate in Cube type");

            for (int i = 0; i < 9; i++)
            {
                sw.WriteLine("libEditor_GV_RWS_GateCountPerCubeType[" + i.ToString() + "] = " + (typeGateCount[i]).ToString() + ";");
            }

            sw.WriteLine("\r\n// Count of Gate in Cube Side");

            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    sw.WriteLine("libEditor_GV_RWS_GateCountPerSide[" + i.ToString() + "][" + j.ToString() + "] = " + (sideGateCount[i][j]).ToString() + ";");
                }
            }

            sw.WriteLine("\r\n// The first index of Gate of Cube side");

            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    if (j < typeGateCount[i])
                    {
                        sw.WriteLine("libEditor_GV_RWS_LinkGateSideStartType[" + i.ToString() + "][" + j.ToString() + "] = " + (faceSideStart[i][j]).ToString() + ";");
                    }
                    else
                    {
                        sw.WriteLine("libEditor_GV_RWS_LinkGateSideStartType[" + i.ToString() + "][" + j.ToString() + "] = -1;");
                    }
                }
            }

            sw.WriteLine("\r\n// The opposite Side index of Gate belong, 0:Bottom, 1:Right, 2:Up, 3:Left");

            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 12; j++)
                {
                    if (j < typeGateCount[i])
                    {
                        sw.WriteLine("libEditor_GV_RWS_OppositeSideIndexForGate[" + i.ToString() + "][" + j.ToString() + "] = " + (oppositesIndex[i][j]).ToString() + ";");
                    }
                    else
                    {
                        break;
                    }

                }
            }

            sw.WriteLine("\r\n// Set Vertex Moving From Center");

            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    sw.WriteLine("libEditor_GV_RWS_CubeVertexMoving[" + i.ToString() + "][" + j.ToString() + "][0] = " + (side[i][0] / 2 * valueFace[j][0]).ToString() + ";");
                    sw.WriteLine("libEditor_GV_RWS_CubeVertexMoving[" + i.ToString() + "][" + j.ToString() + "][1] = " + (side[i][1] / 2 * valueFace[j][1]).ToString() + ";");
                }
            }

            sw.WriteLine("\r\n// Set Gate Moving From Center");

            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 12; j++)
                {
                    if (j < typeGateCount[i])
                    {
                        sw.WriteLine("libEditor_GV_RWS_CubeGateMoving[" + i.ToString() + "][" + j.ToString() + "][0] = " + (side[i][0] * rate[translate[i][j]][0] / 2).ToString() + ";");
                        sw.WriteLine("libEditor_GV_RWS_CubeGateMoving[" + i.ToString() + "][" + j.ToString() + "][1] = " + (side[i][1] * rate[translate[i][j]][1] / 2).ToString() + ";");
                    }
                    else
                    {
                        break;
                    }

                }
            }

            sw.WriteLine("\r\n// Set Gate Test Moving From Center");

            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 12; j++)
                {
                    if (j < typeGateCount[i])
                    {
                        switch (oppositesIndex[i][j])
                        {
                            case 2:
                                sw.WriteLine("libEditor_GV_RWS_CubeGateTestMoving[" + i.ToString() + "][" + j.ToString() + "][0] = " + (side[i][0] * rate[translate[i][j]][0] / 2).ToString() + ";");
                                sw.WriteLine("libEditor_GV_RWS_CubeGateTestMoving[" + i.ToString() + "][" + j.ToString() + "][1] = " + (side[i][1] * rate[translate[i][j]][1] / 2 - 15).ToString() + ";");
                                break;
                            case 3:
                                sw.WriteLine("libEditor_GV_RWS_CubeGateTestMoving[" + i.ToString() + "][" + j.ToString() + "][0] = " + (side[i][0] * rate[translate[i][j]][0] / 2 + 15).ToString() + ";");
                                sw.WriteLine("libEditor_GV_RWS_CubeGateTestMoving[" + i.ToString() + "][" + j.ToString() + "][1] = " + (side[i][1] * rate[translate[i][j]][1] / 2).ToString() + ";");
                                break;
                            case 0:
                                sw.WriteLine("libEditor_GV_RWS_CubeGateTestMoving[" + i.ToString() + "][" + j.ToString() + "][0] = " + (side[i][0] * rate[translate[i][j]][0] / 2).ToString() + ";");
                                sw.WriteLine("libEditor_GV_RWS_CubeGateTestMoving[" + i.ToString() + "][" + j.ToString() + "][1] = " + (side[i][1] * rate[translate[i][j]][1] / 2 + 15).ToString() + ";");
                                break;
                            case 1:
                                sw.WriteLine("libEditor_GV_RWS_CubeGateTestMoving[" + i.ToString() + "][" + j.ToString() + "][0] = " + (side[i][0] * rate[translate[i][j]][0] / 2 - 15).ToString() + ";");
                                sw.WriteLine("libEditor_GV_RWS_CubeGateTestMoving[" + i.ToString() + "][" + j.ToString() + "][1] = " + (side[i][1] * rate[translate[i][j]][1] / 2).ToString() + ";");
                                break;
                            default:
                                break;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
            sw.WriteLine("\n// Set Cube overlap test");
            for (int i = 0; i < 9; i++)
            {
                int l = 0;
                for (double j = (side[i][0] % 14 - side[i][0]) / 2; j < side[i][0] / 2; j += 14)
                {
                    for (double k = (side[i][1] % 14 - side[i][1]) / 2; k < side[i][1] / 2; k += 14)
                    {
                        sw.WriteLine("libEditor_GV_RWS_CubeOverlapTestMoving[" + i.ToString() + "][" + l.ToString() + "][0] = " + j.ToString() + ";");
                        sw.WriteLine("libEditor_GV_RWS_CubeOverlapTestMoving[" + i.ToString() + "][" + l.ToString() + "][1] = " + k.ToString() + ";");
                        l++;
                    }
                }
                sw.WriteLine("libEditor_GV_RWS_CubeOverlapTestCount[" + i.ToString() + "] = " + l.ToString() + ";");
            }
            sw.Close();
        }
    }
}
